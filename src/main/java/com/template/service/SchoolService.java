package com.template.service;

import com.template.model.School;
import org.springframework.stereotype.Service;

@Service
public class SchoolService {

    public School getSchool() {
        School school = new School();
        school.setName("New Your Elementary School");
        school.setHeadmaster("John Doe");
        school.setCapacity(700L);
        school.setNumberoOfClasses(15);
        school.setInDowntown(true);

        System.out.println(school.toString());

        return school;
    }
}
