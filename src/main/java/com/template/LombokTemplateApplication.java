package com.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LombokTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(LombokTemplateApplication.class, args);
	}

}
