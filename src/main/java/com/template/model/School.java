package com.template.model;

import lombok.Data;

@Data
public class School {

    private String name;
    private String headmaster;
    private long capacity;
    private int numberoOfClasses;
    private boolean isInDowntown;
}
